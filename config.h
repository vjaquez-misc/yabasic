/* Define to 1 if you have <alloca.h> and it should be used (not on Ultrix).
   */
#define HAVE_ALLOCA_H 1

/* defined, if ncurses.h is present */
#define HAVE_CURSES_HEADER 1

/* Define to 1 if you have the `getnstr' function. */
#define HAVE_GETNSTR 1

/* Define to 1 if you have the <malloc.h> header file. */
#define HAVE_MALLOC_H 1

/* Define to 1 if you have the `mkstemp' function. */
#define HAVE_MKSTEMP 1

/* defined, if ncurses.h is present */
#define HAVE_NCURSES_HEADER 1

/* Define to 1 if you have the `strerror' function. */
#define HAVE_STRERROR 1

