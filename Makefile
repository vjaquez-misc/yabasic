CFLAGS := -ggdb -Wall -Wextra -Wno-unused-parameter -Wmissing-prototypes -ansi -std=c99

override CFLAGS += -D_GNU_SOURCE

override CFLAGS += -DUNIX -DVERSION="\"2.763\"" -DUNIX_ARCHITECTURE="\"$(shell uname -m)\"" -DBUILD_TIME="\"$(shell date)\""

NCURSES_CFLAGS := $(shell pkg-config --cflags ncurses)
NCURSES_LIBS := $(shell pkg-config --libs ncurses)

yabasic: main.o function.o io.o graphic.o symbol.o lex.yy.o yabasic.tab.o
yabasic: override CFLAGS += -I. $(NCURSES_CFLAGS)
yabasic: override LIBS += -lm -lX11 $(NCURSES_LIBS)
bins += yabasic

yabasic.html: yabasic.xml
	xmlto --skip-validation html-nochunks $<

doc: yabasic.html

all: $(bins)

.PHONY: doc

%.o:: %.c
	$(CC) $(CFLAGS) -MMD -o $@ -c $<

$(bins):
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

lex.yy.c: yabasic.l yabasic.tab.h
	flex -i $<

yabasic.tab.h: yabasic.tab.c

yabasic.tab.c: yabasic.y
	bison -d -t $<

clean:
	$(RM) -v $(bins) lex.yy.c bison.c bison.h *.o *.d yabasic.html

-include *.d
